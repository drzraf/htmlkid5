var app = new Vue({
    el: '#app',
    data: {
        maxPad: 0,
        history: [],
        previous: null,
        current: '',
        op: null,
    },
    computed: {
        showHistory() {
            var res = '';
            for(var [pre, op, post] of this.history) {
                res += `
<tr>
   <td></td>
   <td>${pre}</td>
</tr>
<tr>
  <td data-op="${op}">${op == '/' ? '÷' : op}</td>
  <td>${post}</td>
</tr>
<tr>
  <td class="eq">=</td>
  <td class="eq">${eval(pre + ' ' + (op == '^' ? '**' : op) + ' ' + post)}</td>
</tr>`;
            }
            return res;
        },
        numlength() {
            if (!this.previous && !this.current) {
                return 10;
            }
            return Math.max(Math.log10(this.previous), Math.log10(parseInt(this.current)));
        },
        currentAsString() {
            return this.current.toString().toLocaleString('fullwide', {useGrouping:false});
        },

        total() {
            if (isNaN(this.current)) {
                return '';
            }

            if (!this.op || !this.previous) {
                return this.current;
            }

            if (this.op == '/' && this.current == 0) {
                return '<ERROR>';
            }

            return eval(this.previous + ' ' + (this.op == '^' ? '**' : this.op) + ' ' + this.current).toLocaleString('fullwide', {useGrouping:false});
        },

        digitsDiff() {
                return parseInt(Math.log10(this.previous)) - parseInt(Math.log10(this.current));
        },

        formatOpt() {
            return {
                digitsDiff: this.digitsDiff,
                op: this.op,
                previous: this.previous,
                current: this.current,
                previousAsString: this.previousAsString,
                currentAsString: this.currentAsString
            };
        }
    },

    filters: {
        colorify(value, formatOpt, classcolor = 'green') {
            if (!value) {
                return value;
            }
            var {digitsDiff, op, previous, currentAsString} = formatOpt;

            if (op != '+' && op != '-') {
                return value;
            }

            value = value.toString();
            var textValue = value;

            var i = 0;
            // previous >= current * 10
            if (digitsDiff > 0) {
                while(digitsDiff-- > 0 && value[i] == previous.toString()[i]) {
                    textValue = textValue.replace(/(\d)(\d)/, '<span class="' + classcolor + '">$1</span>$2');
                    i++;
                } 
            } else if (digitsDiff < 0) {
                while(digitsDiff++ < 0 && value[i] == currentAsString[i]) {
                    textValue = textValue.replace(/(\d)(\d)/, '<span class="' + classcolor + '">$1</span>$2');
                    i++;
                }
            }

            // ToDo: colorize decimals if identical

            return textValue;
        }
    },

    methods: {
        handle(e) {
            console.log(e);
            switch(e.key) {
            case "0":
            case "1":
            case "2":
            case "3":
            case "4":
            case "5":
            case "6":
            case "7":
            case "8":
            case "9":
                this.current *= 10;
                this.current += parseInt(e.key);
                return;
            case "Backspace":
                if (this.current) {
                    this.current = parseInt(this.currentAsString.slice(0, -1)) || 0;
                }
                return;
            case "ArrowUp":
                this.current += 1;
                return;
            case "ArrowDown":
                this.current -= 1;
                return;
            case "Delete":
                this.current = 0;
                return;
            case '/':
                e.preventDefault();
            case '*':
            case '-':
            case '+':
            case '^':
                if (this.op == '*' && e.key == '*') {
                    this.op = '^';
                } else {
                    this.op = e.key;
                }

                // Operator with current number : feel free to change the operator
                if (! this.current) {
                    return;
                }

                // First time a number is set and an op is set: still feel free to change the operator.
                // Press enter to validate.
                if (!this.previous) {
                    // return;
                }
            case 'Enter':
                if (!this.op || !this.current) {
                    return;
                }

                // First time a number is validated and an op is set.
                if (!this.previous) {
                    this.previous = this.current;
                    this.current = 0;
                    return;
                }

                // A previous number is stage, an op' is set a current number exists and enter is pressed.
                this.history.push([this.previous, this.op, this.current]);
                this.previous = this.total;
                this.current = 0;
                this.op = null;
                // this.maxPad = Math.max(this.maxPad, Math.log10(this.previous));
            default:
                return;
            }

        }
    }
});
